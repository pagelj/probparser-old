#coding=utf-8

### Import ###

from grammar import Grammar

### Classes ###

class CYK(object):

    def __init__(self, sentence, grammar_object):

        self.grammar = grammar_object.grammar
        self.rootcat = grammar_object.root
        self.grammar_path = grammar_object.grammar_path
        self.sentence = sentence
        self.words = self.sentence.split()
        self.chart = self._cyk()

        # Print grammaticality judgement
        if self._grammatical():

            print('\nThe sentence "'+self.sentence+'" is grammatical!\n')

        else:

            print('\nThe sentence "'+self.sentence+'" is ungrammatical!\n')

    def __str__(self):

        return 'Input sentence: ' + str(self.sentence) + '\n\n' + '\n'.join(str(x)+': '+str(self.chart[x][0]) for x in sorted(self.chart))

    def __initial(self):

        # Function for initializing a 
        # CYK chart out of an input sentence
        # and a given grammar.

        # Create chart dict.
        chart = {}

        # Do for every word ...
        for l in range(1, len(self.words)+1):
        
            # ... and for every grammar rule ...
            for rule in self.grammar:
            
                # ... if the word is in the grammar ...
                if self.words[l-1] in rule: 

                    # ... every word gets its own cell in the chart.
                    # The first word is put into cell (1,1), the second
                    # into cell (2,2) and so on.
                    # As a value, the cell gets its possible Parts of Speech.
                    # Note that the cells (1,1), (2,2) etc. are
                    # the cells for the preterminal nodes, i.e. the POS.
                    # Therefor every POS cell gets a subcell (0,0),
                    # where the concrete form of the word is saved
                    # (i.e. the terminal node).
                    # The format of the value of every cell takes
                    # account of possible ambiguities and references to
                    # its children nodes and is identical for every
                    # cell of the chart.
                    chart[(l,l)] = [{tuple(self.grammar[rule]): [{(0,0) :self.words[l-1]}]}]
                    # A possible filled initial chart looks as follows:
                    # {(1,1): [{('DET',): [{(0,0): 'the'}]}],
                    #  (2,2): [{('N',): [{(0,0): 'men'}]}],
                    #  (3,3): [{('VP','N'): [{(0,0): 'laugh'}]}]}

        # Return the initialized chart.
        return chart


    def _cyk(self):

        # Function for filling a CYK chart.
        # The used algorithm is:

        # Initialize the CYK chart.
        chart = self.__initial()

        # Process the CYK algorithm.
        # The first cell is (i,k),
        # the second cell is (k+1,j)
        # (see CYK algorithm).

        for l in range(2, (len(self.words))+1):

            for i in range(1, (len(self.words)-l+1)+1):

                j = i+l-1

                for k in range(i, (i+l-2)+1):

                    # Iterate over every possible
                    # rule for a cell (necessary
                    # because of ambiguities and
                    # multiple POS for a word).
                    # A subscript 1 is used
                    # for the first cell in variable naming,
                    # subscript 2 for the second cell.

                    if (i,k) in chart:
                    
                        for dicts1 in chart[(i,k)]:

                            for key1 in dicts1:
                            
                                for entry1 in set(key1):

                                    if (k+1,j) in chart:

                                        for dicts2 in chart[(k+1,j)]:

                                            for key2 in dicts2:

                                                for entry2 in set(key2):

                                                    # If the content of the cells
                                                    # is in the grammar:
                                                    if (entry1, entry2) in self.grammar:

                                                        # Fill the target cell (i,j) if the source cells (i,k) and
                                                        # (k+1,j) yield a possible grammar rule.
                                                        # For example:
                                                        # Cell (i,k) contains 'D' and cell (k+1,j) contains 'N'.
                                                        # The grammar contains the entry 'NP -> D N' and therefor, cell
                                                        # (i,j) is filled with the entry 'NP'.

                                                        # A new entry for the cell (i,j) is created.
                                                        # An example entry might look like this:
                                                        # {('S',): [{(1, 2): 'NP'}, {(3, 8): 'VP'}]}
                                                        new_entry = {tuple(self.grammar[(entry1, entry2)]): [{(i,k): entry1}, {(k+1,j): entry2}]}
                                                        
                                                        # If cell (i,j) was created before,
                                                        # additional ambiguities are entered here.

                                                        if (i,j) in chart:

                                                            # Ensure that no double entries are created.
                                                            if new_entry not in chart[(i,j)]:
                                                                chart[(i,j)].extend([new_entry])

                                                            else: continue

                                                        # If cell (i,j) wasn't created before, it is done here.
                                                        else:

                                                            chart[(i,j)] = [new_entry]

                                                    # Continue the algorithm
                                                    # if the cells (i,k) or (k+1,j) were not
                                                    # created before or if a fitting grammar rule
                                                    # could not be found.
                                                    else: continue

                                    else: continue
                                    
                    else: continue

        # End of chart filling #

        # Returning of filled chart.

        # An example chart for the sentence 
        # 'the man sees the woman with the telescope on the hill'
        # might look as following (depending on the used grammar):

        # {(1, 1): [{('DET',): [{(0, 0): 'the'}]}],
        #  (1, 2): [{('NP',): [{(1, 1): 'DET'}, {(2, 2): 'N'}]}],
        #  (1, 5): [{('S',): [{(1, 2): 'NP'}, {(3, 5): 'VP'}]}],
        #  (1, 8): [{('S',): [{(1, 2): 'NP'}, {(3, 8): 'VP'}]}],
        #  (1, 11): [{('S',): [{(1, 2): 'NP'}, {(3, 11): 'VP'}]}],
        #  (2, 2): [{('V', 'N'): [{(0, 0): 'man'}]}],
        #  (3, 3): [{('V',): [{(0, 0): 'sees'}]}],
        #  (3, 5): [{('VP',): [{(3, 3): 'V'}, {(4, 5): 'NP'}]}],
        #  (3, 8): [{('VP',): [{(3, 3): 'V'}, {(4, 8): 'NP'}]}],
        #  (3, 11): [{('VP',): [{(3, 3): 'V'}, {(4, 11): 'NP'}]}],
        #  (4, 4): [{('DET',): [{(0, 0): 'the'}]}],
        #  (4, 5): [{('NP',): [{(4, 4): 'DET'}, {(5, 5): 'N'}]}],
        #  (4, 8): [{('NP',): [{(4, 5): 'NP'}, {(6, 8): 'PP'}]}],
        #  (4, 11): [{('NP',): [{(4, 5): 'NP'}, {(6, 11): 'PP'}]}],
        #  (5, 5): [{('N',): [{(0, 0): 'woman'}]}],
        #  (6, 6): [{('P',): [{(0, 0): 'with'}]}],
        #  (6, 8): [{('PP',): [{(6, 6): 'P'}, {(7, 8): 'NP'}]}],
        #  (6, 11): [{('PP',): [{(6, 6): 'P'}, {(7, 11): 'NP'}]}],
        #  (7, 7): [{('DET',): [{(0, 0): 'the'}]}],
        #  (7, 8): [{('NP',): [{(7, 7): 'DET'}, {(8, 8): 'N'}]}],
        #  (7, 11): [{('NP',): [{(7, 8): 'NP'}, {(9, 11): 'PP'}]}],
        #  (8, 8): [{('N',): [{(0, 0): 'telescope'}]}],
        #  (9, 9): [{('P',): [{(0, 0): 'on'}]}],
        #  (9, 11): [{('PP',): [{(9, 9): 'P'}, {(10, 11): 'NP'}]}],
        #  (10, 10): [{('DET',): [{(0, 0): 'the'}]}],
        #  (10, 11): [{('NP',): [{(10, 10): 'DET'}, {(11, 11): 'N'}]}],
        #  (11, 11): [{('N',): [{(0, 0): 'hill'}]}]}
        
        return chart

    def _grammatical(self):

        # If there is a final cell:
        if (1, len(self.words)) in self.chart:

            # For every entry of the final cell
            # return if the input sentence was grammatical
            # or not.
            for entry in self.chart[1, len(self.words)]:

                if (self.rootcat,) in entry:

                    return True

                # If there is a final cell, but it
                # contains no root node, the sentence
                # is ungrammatical.
                else:

                    return False

        # If there is no final cell, the sentence
        # is ungrammatical.
        else:

            return False
