#coding=utf-8

def fraction2float(fraction):

    if isinstance(fraction, str):

        if "/" in fraction:

            parts = fraction.split('/')

            try:

                return float(parts[0])/float(parts[1])

            except ValueError:

                return fraction

        else:
        
            return fraction

    else:

        return fraction

def isprob(number):

    try:

        if 0 <= float(fraction2float(number)) <= 1:

            return True

        else:

            return False

    except ValueError:

        return False
