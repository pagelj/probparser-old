#coding=utf-8

class Parser(object):

    def __init__(self, sentence, chart):

        self.sentence = sentence
        self.words = self.sentence.split()
        self.chart = chart

    def _parse(self):

        # Funktion, um das Parsing zu initialisieren

        ### Recursive Parsing Function ###

        if (1,len(self.words)) in self.chart:

            # Create parsing dict for recursion.
            dictionary = {(1,len(self.words)): self.chart[1,len(self.words)]}

            for dict_list in dictionary.values():

                for dicts in dict_list:

                    for tupel in dicts:

                        for string in tupel:

                            subconst_val = string

            if string == 'S':

                # The first recursion starts with a count of 0.
                self.__parse_rek(dictionary, subconst_val, 0)

            else:

                pass

        return

    def __parse_rek(self, dictionary, subconst_val, rek_count):

        # subconst_val is the value of a child constituent of the former recursion.
        # rek_count is needed for printing the trees correctly.

        for dict_list in dictionary.values():

            # For every possible phrase in the cell ...

            for dicts in dict_list:

                for tupel in dicts:

                    # ... print the phrase name.

                    for string in tupel:

                        # If there is ambiguity, i.e. multiple possible phrases:
                        if len(dict_list) > 1:

                            print(('\t'*rek_count)+string)

                        else:

                            # If there is ambiguity at POS level:
                            if string == subconst_val:

                                print(('\t'*rek_count)+string)

                # Process recursion for every possible phrase
                # in dict_list. The chart contains the needed
                # references.
                for subconst_list in dicts.values():

                    for subconst_dict in subconst_list:

                        for subconst_key in subconst_dict:

                            subconst_val = subconst_dict[subconst_key]

                            # End recursion if a string is reached.
                            if subconst_key == (0,0):

                                print(('\t'*(rek_count+1))+subconst_dict[subconst_key])

                            # Enter recursion with new dictionary.
                            else:
                        
                                self.__parse_rek({subconst_key: self.chart[subconst_key]}, subconst_val, rek_count+1)

        return
