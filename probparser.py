#/usr/bin/python3
#coding=utf-8

### Import ###

from grammar import Grammar
from cyk import CYK
from parser import Parser

### Global variables ###

sentence1 = "he laughs"
sentence2 = "the man laughs"
sentence3 = "the man sees the woman"
sentence4 = "the man sees the woman with the telescope"
sentence5 = "the man sees the woman with the telescope on the hill"
sentence6 = "the man sees the woman with"
sentence7 = "the man"

"""
sentence8="er lacht"
sentence9="der Mann lacht"
sentence10="der Mann sieht die Frau"
sentence11="der Mann sieht die Frau mit dem Fernglas"
sentence12="der Mann sieht die Frau mit dem Fernglas auf dem Berg"
sentence13="der Mann sieht die Frau mit"
sentence14="der Mann"
sentences_german = (sentence8, sentence9, sentence10, sentence11, sentence12, sentence13, sentence14)
"""

sentences = (sentence1, sentence2, sentence3, sentence4, sentence5, sentence6, sentence7)
grammar_path = 'grammar.txt'


### Main ###

def main():

    # Reading in the grammar.

    grammar_obj = Grammar(grammar_path)

    # Parse all sentences.

    print('\nSome example sentences:')

    for sentence in sentences:

        # Create CYK chart
        # and print grammaticality judgements.
        
        cyk_obj = CYK(sentence, grammar_obj)

        # Parse the sentence.

        Parser(cyk_obj.sentence, cyk_obj.chart)._parse()

    """
    # Users can enter own sentences.
    
    enter = ''

    while enter != 'q()':

        enter = input("\nPlease enter a sentence you wish to parse.\nEnter 'q()' for quitting the program:\n\n")

        if enter != 'q()':

            cyk = CYK(enter, grammar_object)

            # Parse the sentence.

            Parser(cyk.sentence, cyk.chart)._parse()
    """

if __name__=='__main__':

    main()
