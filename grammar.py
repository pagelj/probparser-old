#coding=utf-8

####################################################
################### Import #########################
####################################################

import support

####################################################
################### Classes ########################
####################################################

class Grammar(object):

    def __init__(self, grammar_path):

        self.grammar_path = grammar_path
        self.file_content = open(self.grammar_path, 'r', encoding='utf-8').read()
        self.grammar = self._return_grammar()

    def __str__(self):

        return self.file_content

    def _return_grammar(self):

        # Function for reading in a grammar.
        # Puts every rule into a dictionary.
        # The right-hand side of every rule is a key,
        # the left-hand side its value.

        # Create grammar dict.
        grammar = {}

        if 'ROOT' not in self.file_content:

            print("Error: There is no declaration of 'ROOT' in '"+str(self.grammar_path)+"'")

        # Reading in the grammar.
        infile = open(self.grammar_path, 'r', encoding='utf-8')

        # Process every line of the grammar file.
        line_number=0
        for line in infile:

            line_number+=1

            if line.lstrip().startswith('ROOT'):

                self.root = line.strip('ROOT').lstrip().rstrip().replace('.','')

            elif line == '\n':

                continue

            elif line.startswith('#'):
                
                # Ignore comments.
                continue

            elif ":" in line and "->" in line:
    
                parts = line.rstrip().lstrip().split(':')
                rules = parts[0].rstrip().lstrip().split('->')
                probs = parts[1].rstrip().lstrip().replace("[", "").replace("].", "").replace(",",".")

                if support.isprob(probs):

                    probs = float(support.fraction2float(probs))

                else:

                    print("Error: No valid probability value in line "+str(line_number)+" in '"+str(self.grammar_path)+"': "+'"'+str(line).rstrip('\n')+'"')

                # Initialize the keys and their values.
                value = rules[0].rstrip().lstrip()
                key = tuple(rules[1].rstrip().lstrip().replace(".", "").split())
                
                # Fill dictionary.
                try:

                    grammar[key].extend([value])
                
                except KeyError:

                    grammar[key] = [value]

            else:

                print("Error parsing line "+str(line_number)+" in '"+str(self.grammar_path)+"': "+'"'+str(line).rstrip('\n')+'"')
    
        # Close file.
        infile.close()

        return grammar
