# ProbParser #

A probabilistic parser written in python3 using the [Cocke–Younger–Kasami algorithm (CYK)](https://en.wikipedia.org/wiki/CYK_algorithm) for parsing.

## Usage ##

```
#!shell

$ python3 probparser.py
```

## Format of files ##

### Grammar ###

The parser needs a given grammar file.

The format of the rules is as follows:

* *Left side of rule* -> *Right side of rule*: [*Probability*].\n

* Every rule has to end with a period and a newline.

* The rules have to be in [Chomsky Normal Form](https://en.wikipedia.org/wiki/Chomsky_normal_form).

* After the grammar rule, there is a colon (:) with the probability for this rule in squared brackets ([..])

* The probabilities may be of the following formats:

    * 0.3

    * .3

    * 1/3

    * 0,3

    * 1.0

    * 1

* There has to be a line starting with ROOT and the root categorie following after a space.

* Comments can be made by starting the line with a hash symbol (\#).

An example grammar might look like this:

```
# My little grammar

ROOT S.

# Rules

S -> NP VP: [1].

NP -> D N: [1].

# Lexicon

VP -> laughs: [1].

D -> the: [1].

N -> child: [1].
```

The only sentence which is accepted by this grammar is 'the child laughs',
with the structure

\[S \[NP \[D the\] \[N child\]\] \[VP laughs\]\]
